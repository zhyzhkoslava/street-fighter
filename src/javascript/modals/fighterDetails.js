import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name } = fighter;
  const { health } = fighter;
  const { attack } = fighter;
  const { defense } = fighter;
  const { source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterInfo = createElement({ tagName: 'div', className: 'fighter-info' });
  const nameElement = createElement({ tagName: 'h3', className: 'fighter-name' });
  const sourceElement = createElement({ tagName: 'img', className: 'fighter-source' });
  const attackElement = createElement({ tagName: 'p', className: 'fighter-attack' });
  const healthElement = createElement({ tagName: 'p', className: 'fighter-health' });
  const defenseElement = createElement({ tagName: 'p', className: 'fighter-defense' });


  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  sourceElement.src = source;
  attackElement.innerText = `Health: ${attack}`;
  healthElement.innerText = `Attack: ${health}`;
  defenseElement.innerText = `Defense: ${defense}`;


  fighterDetails.append(nameElement);
  fighterDetails.append(sourceElement);
  fighterDetails.append(fighterInfo);
  fighterInfo.append(healthElement);
  fighterInfo.append(attackElement);
  fighterInfo.append(defenseElement);


  return fighterDetails;
}
