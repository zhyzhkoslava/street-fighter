import {showModal} from "./modal";

export  function showWinnerModal(fighter) {
  // show winner name and imag
    const img = document.createElement('img');
    img.src = fighter.source;

    showModal({
        title: `And winner is ${fighter.name}`,
        bodyElement: img,
    });

    document.querySelector('.close-btn').addEventListener('click', function () {
        location.reload()
    })
}