export function fight(firstFighter, secondFighter) {
    try {
        const firstPlayer = {
            name: firstFighter.name,
            attack: firstFighter.attack,
            defense: firstFighter.defense,
            initialHealth: firstFighter.health,
            currentHealth: firstFighter.health,
            source: firstFighter.source,
            blocking: false
        };
        const secondPlayer = {
            name: secondFighter.name,
            attack: secondFighter.attack,
            defense: secondFighter.defense,
            initialHealth: secondFighter.health,
            currentHealth: secondFighter.health,
            source: secondFighter.source,
            blocking: false
        };


        let winner;

        while (true) {
            let firstAttack = getDamage(firstPlayer, secondPlayer);
            let secondAttack = getDamage(secondPlayer, firstPlayer);

            secondPlayer.currentHealth -= firstAttack;
            firstPlayer.currentHealth -= secondAttack;

            if (firstPlayer.currentHealth <= 0 || secondPlayer.currentHealth <= 0) {
                if (firstPlayer.currentHealth > 0) return winner = firstPlayer;
                return winner = secondPlayer
            }
        }
    } catch (e) {
        throw e
    }
}

export function getDamage(attacker, enemy) {
    if (!enemy.blocking) {
        const damage = getHitPower(attacker) - getBlockPower(enemy);
        return damage > 0 ? damage : 0;
    } else {
        return 0;
    }
}

const getRandomNumber = (min, max) => {
    return Math.random() * (max - min) + min;
};

export function getHitPower(fighter) {
    const criticalHitChance = getRandomNumber(1, 2);
    const {attack} = fighter;
    return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance = getRandomNumber(1, 2);
    const {defense} = fighter;
    return defense * dodgeChance;
}
